# RecSys

This application is a end to end movie recommendation system.

## Modules

- Frontend wechat mini program 
- Full tracking data collection 
- Offline recommendation candidate generating
- Offline learning to rank modeling
- Near-line feature extraction
- High availability backend recommendation service
- Completed A/B test experiments management

## Tech stack
### Programming Language
java, python, scala, javascript, nodejs

### Front-end
angularjs, bootstrap4
  
### Back-end
spring-boot, spring-security junit, mockito,  mysql,
 
### BigData
spark, kafka, hbase

### Tools 
gradle, docker, docker-compose, sonarqube, jenkins, travis, webpack, swagger



## Development

To start your application in the dev profile, simply run:

    


For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

### Using angular-cli

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

    ng generate component my-component

will generate few files:

    create webapp/main/app/my-component/my-component.component.html
    create webapp/main/app/my-component/my-component.component.ts
    update webapp/main/app/app.module.ts


## Building for production

To optimize the recsys application for production, run:


To ensure everything worked, run:



Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./gradlew test
### Other tests

Performance tests are run by [Gatling][] and written in Scala. They're located in [src/test/gatling](src/test/gatling).

To use those tests, you must install Gatling from [https://gatling.io/](https://gatling.io/).

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

Then, run a Sonar analysis:

```
./gradlew -Pprod clean test sonarqube
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [docker](docker) folder to launch required third party services.

For example, to start a  database in a docker container, run:

    docker-compose -f docker/.yml up -d

To stop it and remove the container, run:

    docker-compose -f /docker/.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    

Then run:

    docker-compose -f docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

[JHipster Homepage and latest documentation]: https://www.jhipster.tech
[JHipster 5.6.0 archive]: https://www.jhipster.tech/documentation-archive/v5.6.0

[Using JHipster in development]: https://www.jhipster.tech/documentation-archive/v5.6.0/development/
[Using Docker and Docker-Compose]: https://www.jhipster.tech/documentation-archive/v5.6.0/docker-compose
[Using JHipster in production]: https://www.jhipster.tech/documentation-archive/v5.6.0/production/
[Running tests page]: https://www.jhipster.tech/documentation-archive/v5.6.0/running-tests/
[Code quality page]: https://www.jhipster.tech/documentation-archive/v5.6.0/code-quality/
[Setting up Continuous Integration]: https://www.jhipster.tech/documentation-archive/v5.6.0/setting-up-ci/

[Gatling]: http://gatling.io/
