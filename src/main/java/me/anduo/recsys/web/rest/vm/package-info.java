/**
 * View Models used by Spring MVC REST controllers.
 */
package me.anduo.recsys.web.rest.vm;
